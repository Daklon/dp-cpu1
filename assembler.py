import sys

opcode_dict = {'j' : {'binopcode':'100000','type':1},
        'jz' : {'binopcode':'100001','type':1},
        'jnz' : {'binopcode':'100010','type':1},
        'li' : {'binopcode':'1100','type':2},
        'nop' : {'binopcode':'1001110000000000','type':0},
        'add' : {'binopcode':'0000','type':3},
        'sub' : {'binopcode':'0001','type':3},
        'and' : {'binopcode':'0010','type':3},
        'or' : {'binopcode':'0011','type':3},
        'neg' : {'binopcode':'10110100','type':4},
        'mina' : {'binopcode':'10111000','type':4},
        'geta' : {'binopcode':'10111100','type':4},
        'call' : {'binopcode':'100011','type':1},
        'ret' : {'binopcode':'1001000000000000','type':0},
        'sid' : {'binopcode':'100101','type':1},
        'idis' : {'binopcode':'1001100000000000','type':0},
	'ien' : {'binopcode':'0101000000000000','type':0},
        'shflr' : {'binopcode':'10101000','type':4},
        'shfll' : {'binopcode':'10101100','type':4},
        'shfar' : {'binopcode':'10110000','type':4},
        'in' : {'binopcode':'101000','type':5},
        'out' : {'binopcode':'101001','type':5}
}

registers = {'r0' : '0000',
            'r1' : '0001',
            'r2' : '0010',
            'r3' : '0011',
            'r4' : '0100',
            'r5' : '0101',
            'r6' : '0110',
            'r7' : '0111',
            'r8' : '1000',
            'r9' : '1001',
            'r10' : '1010',
            'r11' : '1011',
            'r12' : '1100',
            'r13' : '1101',
            'r14' : '1110',
            'r15' : '1111',
}

#not all ports are mapped
ports = {'p0' : '000000',
        'p1' : '000001',
        'p2' : '000010',
        'p3' : '000011',
        'p4' : '000100',
        'p5' : '000101',
        'p6' : '000110',
        'p7' : '000111',
        'p8' : '001000',
        'p9' : '001001',
        'p10' : '001010',
        'p11' : '001011',
        'p12' : '001100',
        'p13' : '001101',
        'p14' : '001110',
        'p15' : '001111',
}

tags = {}

tagline = ""

#jumps, only one parameter, the jump destination adress
def type1_instruction(args):
    global tagline
    tagline = "(DEST: " + str(tags[args]) + ")" #saves the tag destination, to be able to print it as a comment
    return bin(int(tags[args]))[2:].zfill(10)

#load inmediate, two paremeters, the 8 bits inmediate and the 4 bit register destination address
def type2_instruction(args):
    data = args.split(",")
    #print(data[0])
    #print(int(data[0]))
    #print(bin(int(data[0])))
    #print(bin(int(data[0]))[2:])
    #print(bin(int(data[0]))[2:].zfill(8))
    assembledline = bin(int(data[0]))[2:].zfill(8)
    assembledline += registers[data[1]]
    return assembledline

#aluop, three paremeters, r1,r2 and rd
def type3_instruction(args):
    data = args.split(",")
    assembledline = registers[data[0]]
    assembledline += registers[data[1]]
    assembledline += registers[data[2]]
    return assembledline

def type4_instruction(args):
    data = args.split(",")
    assembledline = registers[data[0]]
    assembledline += registers[data[1]]
    return assembledline

def type5_instruction(args):
    data = args.split(",")
    assembledline = ports[data[0]]
    assembledline += registers[data[1]]
    return assembledline

def get_assembled(line):
    splited = line.split(" ")
    readed_opcode = splited[0].lower()
    try:
        args = splited[1]
    except: #opcode has no parameters
        pass
    if readed_opcode in opcode_dict:
        assembledline = opcode_dict[readed_opcode]['binopcode']
        if opcode_dict[readed_opcode]['type'] == 0:
            #its a nop, its already writted since the opcode fills the instruction
            pass
        elif opcode_dict[readed_opcode]['type'] == 1:
            assembledline += type1_instruction(args)
        elif opcode_dict[readed_opcode]['type'] == 2:
            assembledline += type2_instruction(args)
        elif opcode_dict[readed_opcode]['type'] == 3:
            assembledline += type3_instruction(args)
        elif opcode_dict[readed_opcode]['type'] == 4:
            assembledline += type4_instruction(args)
        elif opcode_dict[readed_opcode]['type'] == 5:
            assembledline += type5_instruction(args)
    elif readed_opcode[:2] == "//":
        return ''
    elif readed_opcode[:1] == ":":
        return ''
    else:
        raise Exception('ilegal opcode "' + readed_opcode+'"')
    return assembledline


def assemble(infile):
    file = open(infile,'r')
    global tagline
    counter = 0
    for iteration,line in enumerate(file):
        if line[0] == ':':
            line = line.replace("\n","")
            line = line.replace(":","")
            tags.update({line:counter})
        elif line[0] != '/':
            counter = counter+1

    file.seek(0)
    counter = 0
    for iteration,line in enumerate(file):
        line = line.replace("\n","")#eliminamos el caracter de fin de linea
        assembled = get_assembled(line)
        if assembled != '':
            linesplitted = line.split("//")
            if len(linesplitted) > 1:
                print(assembled+ " // " + str(counter) + ". " + linesplitted[0] + " " + str(tagline) + " " + linesplitted[1])
            else:
                print(assembled+ " // " + str(counter) + ". " + linesplitted[0] + " " + str(tagline))
            tagline = ""
            counter = counter+1

    if (counter < 1023):
        for _ in range(1023-counter):
            counter += 1
            print(get_assembled("nop")+ " // " + str(counter)+". "+ "nop")
    
if __name__ == "__main__":
 assemble(sys.argv[1])

