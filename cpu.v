module cpu(input wire clk, reset,int,
input wire [7:0] external_wire_in_p0,
input wire [7:0] external_wire_in_p1,
input wire [7:0] external_wire_in_p2,
input wire [7:0] external_wire_in_p3,
output wire [7:0] external_wire_out_p4,
output wire [7:0] external_wire_out_p5,
output wire [7:0] external_wire_out_p6,
output wire [7:0] external_wire_out_p7);
//Procesador sin memoria de datos de un solo ciclo

wire s_inm;
wire s_inc;
wire we3;
wire wez;
wire z;
wire s_ena;
wire s_rw;
wire s_int;
wire s_int_ena;
wire s_s_sel;
wire s_e_sel;
wire s_w_out;
wire [3:0] alu_op;
wire [5:0] opcode;
wire timer_int;
wire or01_out_int;
reg [7:0] not_in0;

always @(external_wire_in_p0)
begin
    not_in0 <= ~external_wire_in_p0;
end

cd datapath(clk,reset,s_inc,s_inm,we3,wez,s_ena,s_rw,s_int,s_int_ena,s_w_out,s_e_sel,s_s_sel,alu_op,
not_in0,
external_wire_in_p1,
external_wire_in_p2,
external_wire_in_p3,
external_wire_out_p4,
external_wire_out_p5,
external_wire_out_p6,
external_wire_out_p7,
z,opcode);

uc unidadControl(opcode,z,clk,or01_out_int,s_inc,s_inm,we3,wez,s_ena,s_rw,s_int,s_int_ena,s_s_sel,s_e_sel,s_w_out,alu_op);

//Esto en teoría está fuera, pero para simplificar lo he metido dentro, MEJORABLE
timer tm(clk,external_wire_out_p4[7],external_wire_out_p4[6:0],timer_int);
or or01(or01_out_int,timer_int,int);

endmodule
