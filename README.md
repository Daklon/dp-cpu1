1- La codificación elegida se puede ver en el fichero llamado instructionSet, junto con una explicación de cada instrucción.
2- Todas las instrucciones han sido comprobadas con los diferentes programas de ejemplo que se adjuntan.
3- He implementado una unidad de control cableada, pero el código ha sido modelado por comportamiento.
4- La mayor dificultad que he encontrado ha sido el debugging, sobretodo al principio cuando verilog fallaba al compilar.
