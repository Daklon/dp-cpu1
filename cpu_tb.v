`timescale 1 ns / 10 ps

module cpu_tb;


reg clk, reset;
reg int = 1'b0;
reg [7:0] p0 = 8'b00000000;
reg [7:0] p1 = 8'b00000000;
reg [7:0] p2 = 8'b00000000;
reg [7:0] p3 = 8'b00000000;
wire[7:0] p4,p5,p6,p7;

// generación de reloj clk
always //siempre activo, no hay condición de activación
begin
  clk = 1'b1;
  #30;
  clk = 1'b0;
  #30;
end

always
begin
    #(10*60);
    int = 1'b1;
    #60;
    int = 1'b0;
end

// instanciación del procesador
cpu micpu(clk, reset,int,p0,p1,p2,p3,p4,p5,p6,p7);

initial
begin
  $dumpfile("cpu_tb.vcd");
  $dumpvars;
  reset = 1;  //a partir del flanco de subida del reset empieza el funcionamiento normal
  #10;
  reset = 0;  //bajamos el reset 
end

initial
begin

  #(20*60);  //Esperamos 20 ciclos o 20 instrucciones
  $finish;
end

endmodule
