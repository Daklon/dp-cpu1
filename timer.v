module timer #(parameter N = 500000)
              (input wire clk, reset, input wire [6:0]set, output reg alarm);
//clk = 50mhz => 0.02 microseg/tick
//1 s = 100 cs
//0.5 s = 50 cs
//0.5 s = 5000 microseg

//500000 N= para clk = 50mhz, timer cuenta en cs

reg [6:0] r_alarm;
reg [18:0] r_reg;
wire [18:0] r_nxt;
//reg [18:0] r_N = 19'b1111010000100100000; // 500000
reg [18:0] r_N = 'b0111101000010010000; //250000
//reg [18:0] r_N = 19'b0000000000000000100; // 4
reg [6:0] r_final_countdown = 0;
wire [6:0] r_nxt_final_countdown;

always @(posedge clk)
begin
  if (reset == 1'b1)
  begin
      r_alarm <= set;
      r_reg <= 0;
      r_final_countdown <= 0;
      alarm <= 1'b0;
  end
  else
  begin
      if (r_reg == r_N)
      begin
          r_reg <= 0;
          r_final_countdown <= r_nxt_final_countdown;
          if (r_alarm == r_final_countdown)
          begin
              r_final_countdown <=0;
              alarm = 1'b1;
          end
          else
          begin
              alarm = 1'b0;
          end
      end
      else
      begin 
          r_reg <= r_nxt;
      end
  end
end
 
assign r_nxt = r_reg+1'b1;
assign r_nxt_final_countdown = r_final_countdown+1'b1;     

endmodule
