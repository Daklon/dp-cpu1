module uc(input wire [5:0] opcode, input wire z, input wire clk, input wire int, output reg s_inc, s_inm, we3, wez, s_ena, s_rw, s_int, s_int_ena, s_s_sel, s_e_sel, s_w_out, output reg [3:0] op_alu);

reg int_ena;

initial
begin
	int_ena = 1'b0;
end

always @(negedge clk)
begin
  if (int_ena == 1'b1 & int == 1'b1)
    begin
    s_inc = 1'b1;
    s_inm = 1'b?;
    we3 = 1'b0;
    wez = 1'b0;
    s_ena = 1'b1;
    s_rw = 1'b1;
    s_int = 1'b1;
    s_int_ena = 1'b0;
    op_alu = 4'b????;
    s_s_sel = 1'b0;
    s_e_sel = 1'b0;
    s_w_out = 1'b0;
    int_ena = 1'b0;
    end
  else
    begin
    s_int = 1'b0;  
    casez(opcode)
        6'b100111: //nop//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b100100: //ret
            begin
            s_inc = 1'b?;
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b1;
            s_rw = 1'b0;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b100101: //sid
            begin
            int_ena = 1'b0;
            s_inc = 1'b1;
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b0;
            s_int_ena = 1'b1;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b0101??: //ien
            begin
            int_ena = 1'b1;
            s_inc = 1'b1;
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b0;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b100110: //idis
            begin
            int_ena = 1'b0;
            s_inc = 1'b1;
            s_inm = 1'b1;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b100000://j//done
            begin
            s_inc = 1'b0;
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b100001://jz//done
            begin
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            if (z == 1'b1)
                s_inc = 1'b0;
            else
                begin
                s_inc = 1'b1;
                end
            end
        6'b100010://jnz//done
            begin
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            if (z == 1'b0)
                s_inc = 1'b0;
            else
                begin
                s_inc = 1'b1;
                end
            end
        6'b100011://call//done?
            begin
            s_inc = 1'b0;
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b1;
            s_rw = 1'b1;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b1100??://li//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b1;
            we3 = 1'b1;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b0000??://add//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b0010;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b0001??://sub//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b0011;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b0010??://AND//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b0100;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b0011??://OR//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b0101;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end 
        6'b101101://NEG//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b0001;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b101110://MINA//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b1011;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b101111://GETA//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b0000;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b101010://shflr//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b0111;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end 
        6'b101011://shfll//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b1000;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b101100://shfar//done
            begin
            s_inc = 1'b1;
            s_inm = 1'b0;
            we3 = 1'b1;
            wez = 1'b1;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b1001;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b101000://in//done, not tested
            begin
            s_inc = 1'b1;
            s_inm = 1'b1;
            we3 = 1'b1;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b?;
            s_e_sel = 1'b1;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
        6'b101001://out//done, not tested
            begin
            s_inc = 1'b1;
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b1;
            s_e_sel = 1'b?;
            s_w_out = 1'b1;
            s_int = 1'b0;
            end
	default
	    begin
            s_inc = 1'b1;
            s_inm = 1'b?;
            we3 = 1'b0;
            wez = 1'b0;
            s_ena = 1'b0;
            s_rw = 1'b?;
            s_int_ena = 1'b0;
            op_alu = 4'b????;
            s_s_sel = 1'b0;
            s_e_sel = 1'b0;
            s_w_out = 1'b0;
            s_int = 1'b0;
            end
    endcase
    end
end
endmodule
