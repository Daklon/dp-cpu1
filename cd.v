module cd(input wire clk, reset, s_inc, s_inm, we3, wez, s_ena, s_rw, s_int,s_int_ena,s_w_out,s_e_sel,s_s_sel, input wire [3:0] op_alu,
input wire [7:0] external_wire_in_p0,
input wire [7:0] external_wire_in_p1,
input wire [7:0] external_wire_in_p2,
input wire [7:0] external_wire_in_p3,
output wire [7:0] external_wire_out_p4,
output wire [7:0] external_wire_out_p5,
output wire [7:0] external_wire_out_p6,
output wire [7:0] external_wire_out_p7,
output wire z, output wire [5:0] opcode);
//Camino de datos de instrucciones de un solo ciclo

reg [9:0] one = 10'b0000000001;
reg [9:0] error = 10'b1111111111;

wire [9:0] pc_out;
wire [9:0] pc_in;
wire [9:0] mux01_out;
wire [9:0] mux03_out;
wire [9:0] mux04_out;
wire [9:0] sum_out;
wire [9:0] stack_out;
wire [15:0] prog_mem_out;
wire [7:0] wd3;
wire [7:0] rd1;
wire [7:0] rd2;
wire [7:0] alu_out;
wire [9:0] int_out;
wire [7:0] es_out;
wire [7:0] mux06_out;
wire [3:0] mux07_out;
wire s_err;
wire z_alu;
wire and01_out;
//debug

always @(reset)
begin
    one <= 10'b0000000001;
    error = 10'b1111111111;
end

registro #(10) pc(clk,reset,pc_in,pc_out);//done
memprog memoriaprograma(clk,pc_out,prog_mem_out);//done
mux2 #(10) mux01(prog_mem_out[9:0],sum_out,s_inc,mux01_out);//done
mux2 mux02(alu_out,mux06_out,s_inm,wd3);//done
mux2 #(10) mux03(mux01_out,stack_out,and01_out,mux03_out);//done
mux2 #(10) mux04(mux03_out,int_out,s_int,mux04_out);//done
mux2 #(10) mux05(mux04_out,error,s_err,pc_in);//done
mux2 mux06(prog_mem_out[11:04],es_out,s_e_sel,mux06_out);
mux2 #(4) mux07(prog_mem_out[07:04],prog_mem_out[03:00],s_s_sel,mux07_out);
sum sum01(one,pc_out,sum_out);//done
regfile bancoregistros(clk,we3,prog_mem_out[11:08],mux07_out,prog_mem_out[03:00],wd3,rd1,rd2);
alu alu01(rd1,rd2,op_alu,alu_out,z_alu);//done
ffd zero(clk,reset,z_alu,wez,z);//done
ffd #(10) int(clk,reset,prog_mem_out[09:00],s_int_ena,int_out);
stack #(10,9) stck01(clk,reset,stack_out,sum_out,s_ena,s_rw,s_err);//done
entrada entrada01(prog_mem_out[09:04],es_out,external_wire_in_p0,external_wire_in_p1,external_wire_in_p2,external_wire_in_p3);
salida salida01(clk,reset,s_w_out,prog_mem_out[09:04],rd2,external_wire_out_p4,external_wire_out_p5,external_wire_out_p6,external_wire_out_p7);


//puerta que controla el mux03 según las señales de la alu
and and01(and01_out,~s_rw,s_ena);//solo devuelve 1 cuando vamos a hacer una lectura del stack

assign opcode = prog_mem_out[15:10];

endmodule
