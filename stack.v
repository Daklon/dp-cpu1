module stack #(parameter WIDTH = 10,parameter DEPTH = 20)(input wire clk, input wire reset,
            output reg [WIDTH - 1:0] data_out,
            input wire [WIDTH - 1:0] data_in,
            input wire s_ena, input wire s_rw,//s_rw = 0 => read, s_rw = 1 => write
            output reg s_err);

//creamos un puntero y un banco de registros
	reg [DEPTH - 1:0] ptr;
	reg [WIDTH - 1:0] stack [0:(1 << DEPTH) - 1];

    //segun la señal subimos o bajamos el puntero
	always @(posedge clk)
        begin
            data_out = stack[ptr];
	    if (reset)
            begin
                ptr = DEPTH-1;
            end
            else if (s_ena) 
            begin
                if (s_rw==0) 
                begin//pop(read)
                    ptr = ptr + 1;
                    data_out = stack[ptr];
                end
                else//push(write)
                begin
                    ptr = ptr-1;
                    stack[ptr] = data_in;
                    data_out = stack[ptr];
                end
            end
            else
            begin
                data_out = stack[ptr];
            end
            if (ptr==0)
                s_err = 1'b1;
            else if (ptr == DEPTH)
                s_err = 1'b1;
            else
                s_err = 1'b0;
	    end

endmodule
