module entrada (input wire [05:00] address, output wire [07:00] data_out,
input wire [7:0] external_wire_in_p0, 
input wire [7:0] external_wire_in_p1, 
input wire [7:0] external_wire_in_p2, 
input wire [7:0] external_wire_in_p3); 

wire mux01_out;

mux4 mux01(external_wire_in_p0,external_wire_in_p1,external_wire_in_p2,external_wire_in_p3,address[1:0],data_out);

endmodule


module deco1(input wire s_w_out, input wire [05:00] address, output wire out00, output wire out01, output wire out02, output wire out03);

assign {out00,out01,out02,out03} = ({s_w_out,address} == 7'b1000100) ? 4'b1000 :
                                   ({s_w_out,address} == 7'b1000101) ? 4'b0100 :
                                   ({s_w_out,address} == 7'b1000110) ? 4'b0010 :
                                   ({s_w_out,address} == 7'b1000111) ? 4'b0001 :
                                                                       4'b0000;

endmodule

module salida (input wire clk, input wire reset, input wire s_w_out, input wire [05:00] address, input wire [07:00] data_in,
output wire [7:0] external_wire_out_p4,
output wire [7:0] external_wire_out_p5,
output wire [7:0] external_wire_out_p6,
output wire [7:0] external_wire_out_p7);

wire carga0;
wire carga1;
wire carga2;
wire carga3;

deco1 deco01(s_w_out, address, carga0, carga1, carga2, carga3);

ffd #(8) reg0(clk,reset,data_in,carga0,external_wire_out_p4);
ffd #(8) reg1(clk,reset,data_in,carga1,external_wire_out_p5);
ffd #(8) reg2(clk,reset,data_in,carga2,external_wire_out_p6);
ffd #(8) reg3(clk,reset,data_in,carga3,external_wire_out_p7);

endmodule
