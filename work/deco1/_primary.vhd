library verilog;
use verilog.vl_types.all;
entity deco1 is
    port(
        s_w_out         : in     vl_logic;
        address         : in     vl_logic_vector(5 downto 0);
        out00           : out    vl_logic;
        out01           : out    vl_logic;
        out02           : out    vl_logic;
        out03           : out    vl_logic
    );
end deco1;
