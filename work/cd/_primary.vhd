library verilog;
use verilog.vl_types.all;
entity cd is
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        s_inc           : in     vl_logic;
        s_inm           : in     vl_logic;
        we3             : in     vl_logic;
        wez             : in     vl_logic;
        s_ena           : in     vl_logic;
        s_rw            : in     vl_logic;
        s_int           : in     vl_logic;
        s_int_ena       : in     vl_logic;
        s_w_out         : in     vl_logic;
        s_e_sel         : in     vl_logic;
        s_s_sel         : in     vl_logic;
        op_alu          : in     vl_logic_vector(3 downto 0);
        external_wire_in_p0: in     vl_logic_vector(7 downto 0);
        external_wire_in_p1: in     vl_logic_vector(7 downto 0);
        external_wire_in_p2: in     vl_logic_vector(7 downto 0);
        external_wire_in_p3: in     vl_logic_vector(7 downto 0);
        external_wire_out_p4: out    vl_logic_vector(7 downto 0);
        external_wire_out_p5: out    vl_logic_vector(7 downto 0);
        external_wire_out_p6: out    vl_logic_vector(7 downto 0);
        external_wire_out_p7: out    vl_logic_vector(7 downto 0);
        z               : out    vl_logic;
        opcode          : out    vl_logic_vector(5 downto 0)
    );
end cd;
