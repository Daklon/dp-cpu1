library verilog;
use verilog.vl_types.all;
entity salida is
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        s_w_out         : in     vl_logic;
        address         : in     vl_logic_vector(5 downto 0);
        data_in         : in     vl_logic_vector(7 downto 0);
        external_wire_out_p4: out    vl_logic_vector(7 downto 0);
        external_wire_out_p5: out    vl_logic_vector(7 downto 0);
        external_wire_out_p6: out    vl_logic_vector(7 downto 0);
        external_wire_out_p7: out    vl_logic_vector(7 downto 0)
    );
end salida;
