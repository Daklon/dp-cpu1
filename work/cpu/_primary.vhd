library verilog;
use verilog.vl_types.all;
entity cpu is
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        int             : in     vl_logic;
        external_wire_in_p0: in     vl_logic_vector(7 downto 0);
        external_wire_in_p1: in     vl_logic_vector(7 downto 0);
        external_wire_in_p2: in     vl_logic_vector(7 downto 0);
        external_wire_in_p3: in     vl_logic_vector(7 downto 0);
        external_wire_out_p4: out    vl_logic_vector(7 downto 0);
        external_wire_out_p5: out    vl_logic_vector(7 downto 0);
        external_wire_out_p6: out    vl_logic_vector(7 downto 0);
        external_wire_out_p7: out    vl_logic_vector(7 downto 0)
    );
end cpu;
