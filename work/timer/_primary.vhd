library verilog;
use verilog.vl_types.all;
entity timer is
    generic(
        N               : integer := 500000
    );
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        set             : in     vl_logic_vector(6 downto 0);
        alarm           : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of N : constant is 1;
end timer;
