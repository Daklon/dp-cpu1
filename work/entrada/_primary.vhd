library verilog;
use verilog.vl_types.all;
entity entrada is
    port(
        address         : in     vl_logic_vector(5 downto 0);
        data_out        : out    vl_logic_vector(7 downto 0);
        external_wire_in_p0: in     vl_logic_vector(7 downto 0);
        external_wire_in_p1: in     vl_logic_vector(7 downto 0);
        external_wire_in_p2: in     vl_logic_vector(7 downto 0);
        external_wire_in_p3: in     vl_logic_vector(7 downto 0)
    );
end entrada;
