library verilog;
use verilog.vl_types.all;
entity stack is
    generic(
        WIDTH           : integer := 10;
        DEPTH           : integer := 20
    );
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        data_out        : out    vl_logic_vector;
        data_in         : in     vl_logic_vector;
        s_ena           : in     vl_logic;
        s_rw            : in     vl_logic;
        s_err           : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of WIDTH : constant is 1;
    attribute mti_svvh_generic_type of DEPTH : constant is 1;
end stack;
