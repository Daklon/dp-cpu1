EESchema Schematic File Version 4
LIBS:camino de datos-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L symbols:alu sum
U 1 1 5C914F46
P 4600 3450
F 0 "sum" H 4600 3865 50  0001 C CNN
F 1 "sum01" H 4600 3774 50  0000 C CNN
F 2 "" H 4600 3450 50  0001 C CNN
F 3 "" H 4600 3450 50  0001 C CNN
	1    4600 3450
	1    0    0    -1  
$EndComp
$Comp
L symbols:PC U?
U 1 1 5C9151C1
P 4550 4600
F 0 "U?" H 4550 5015 50  0001 C CNN
F 1 "PC" H 4550 4923 50  0000 C CNN
F 2 "" H 4550 4600 50  0001 C CNN
F 3 "" H 4550 4600 50  0001 C CNN
	1    4550 4600
	1    0    0    -1  
$EndComp
$Comp
L symbols:memprog U?
U 1 1 5C9152D0
P 5850 4600
F 0 "U?" H 5850 5015 50  0001 C CNN
F 1 "memoriaprograma" H 5850 4923 50  0000 C CNN
F 2 "" H 5850 4600 50  0001 C CNN
F 3 "" H 5850 4600 50  0001 C CNN
	1    5850 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4600 4900 4600
Text Label 5100 4600 0    50   ~ 0
10b
$Comp
L symbols:bancoregistros U?
U 1 1 5C915506
P 8350 3850
F 0 "U?" H 8350 4275 50  0001 C CNN
F 1 "bancoregistros" H 8350 4184 50  0000 C CNN
F 2 "" H 8350 3850 50  0001 C CNN
F 3 "" H 8350 3850 50  0001 C CNN
	1    8350 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4600 4900 3600
Connection ~ 4900 4600
Wire Wire Line
	4900 4600 5600 4600
Wire Wire Line
	4750 3300 5200 3300
Text Notes 5300 3300 0    50   ~ 0
1
Text Label 4900 3300 0    50   ~ 0
10b
$Comp
L symbols:mux U?
U 1 1 5C915683
P 3750 3150
F 0 "U?" H 3750 3565 50  0001 C CNN
F 1 "mux01" H 3750 3474 50  0000 C CNN
F 2 "" H 3750 3150 50  0001 C CNN
F 3 "" H 3750 3150 50  0001 C CNN
	1    3750 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3450 4350 3450
Text Label 4100 3300 0    50   ~ 0
10b
Wire Wire Line
	6100 4600 6400 4600
Wire Wire Line
	6400 3000 4400 3000
Wire Wire Line
	7500 4000 7950 4000
$Comp
L symbols:mux U?
U 1 1 5C915B8F
P 9600 4150
F 0 "U?" H 9600 4565 50  0001 C CNN
F 1 "mux02" H 9600 4474 50  0000 C CNN
F 2 "" H 9600 4150 50  0001 C CNN
F 3 "" H 9600 4150 50  0001 C CNN
	1    9600 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 4150 9150 4150
Wire Wire Line
	9150 4150 9150 4000
Wire Wire Line
	9150 4000 8750 4000
Wire Wire Line
	10050 4300 9750 4300
Wire Wire Line
	8900 3750 8900 3350
Wire Wire Line
	8900 3350 9150 3350
Wire Wire Line
	9450 3200 10000 3200
Wire Wire Line
	10000 3200 10000 4000
Wire Wire Line
	10000 4000 9750 4000
$Comp
L symbols:alu_0 U?
U 1 1 5C91746A
P 9300 3200
F 0 "U?" H 9300 3616 50  0001 C CNN
F 1 "alu01" H 9300 3525 50  0000 C CNN
F 2 "" H 9300 3200 50  0001 C CNN
F 3 "" H 9300 3200 50  0001 C CNN
	1    9300 3200
	-1   0    0    -1  
$EndComp
Text Label 5150 3000 0    50   Italic 10
prog_mem_out[9:0]
Text Label 3050 3150 0    50   Italic 10
mux01_out
Wire Wire Line
	4900 3600 4750 3600
Text Label 4000 3400 0    50   Italic 10
sum_out
Wire Wire Line
	4350 3450 4350 3300
Wire Wire Line
	4350 3300 3900 3300
Text Label 7500 3700 0    50   ~ 0
RA1[11:08]
Text Label 7500 3850 0    50   ~ 0
RA2[07:04]
Text Label 7500 4000 0    50   ~ 0
WA3[03:00]
Text Label 7900 4500 0    50   ~ 0
INM[11:04]
Text Label 6150 4600 0    50   ~ 0
16b
Text Label 9650 3200 0    50   Italic 10
alu_out
Text Label 9200 4150 0    50   Italic 10
wd3
Wire Wire Line
	8750 3650 8850 3650
Wire Wire Line
	8850 3650 8850 3050
Wire Wire Line
	8850 3050 9150 3050
Text Label 8850 3050 0    50   Italic 10
rd1
Text Label 8900 3350 0    50   Italic 10
rd2
Text Label 4750 4700 0    50   Italic 10
pc_out
$Comp
L symbols:mux U?
U 1 1 5C91CCE0
P 2750 3300
F 0 "U?" H 2750 3816 50  0001 C CNN
F 1 "mux03" H 2750 3725 50  0000 C CNN
F 2 "" H 2750 3300 50  0001 C CNN
F 3 "" H 2750 3300 50  0001 C CNN
	1    2750 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 3150 2900 3150
Text Label 2350 3700 0    50   Italic 10
mux03_out
$Comp
L symbols:stack U?
U 1 1 5C91D569
P 3500 4000
F 0 "U?" H 3829 4046 50  0001 L CNN
F 1 "stck01" H 3829 4000 50  0000 L CNN
F 2 "" H 3500 4000 50  0001 C CNN
F 3 "" H 3500 4000 50  0001 C CNN
	1    3500 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4000 3800 4000
Wire Wire Line
	3800 3450 2900 3450
Wire Wire Line
	3800 3450 3800 4000
Wire Wire Line
	4350 3450 4350 4500
Wire Wire Line
	4350 4500 3150 4500
Wire Wire Line
	3150 4500 3150 4000
Wire Wire Line
	3150 4000 3300 4000
Connection ~ 4350 3450
Text Label 3150 3450 0    50   Italic 10
stack_out
$Comp
L symbols:mux U?
U 1 1 5C91E4F2
P 2200 4050
F 0 "U?" V 2246 4429 50  0001 L CNN
F 1 "mux04" V 2200 4429 50  0000 L CNN
F 2 "" H 2200 4050 50  0001 C CNN
F 3 "" H 2200 4050 50  0001 C CNN
	1    2200 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2600 3300 2350 3300
Wire Wire Line
	2350 3300 2350 3900
Wire Wire Line
	2050 3900 2050 3600
Wire Wire Line
	1550 3600 2050 3600
Text Label 1600 3600 0    50   Italic 10
int_out
$Comp
L symbols:mux U?
U 1 1 5C9202A2
P 2700 4600
F 0 "U?" H 2700 5116 50  0001 C CNN
F 1 "mux05" H 2700 5025 50  0000 C CNN
F 2 "" H 2700 4600 50  0001 C CNN
F 3 "" H 2700 4600 50  0001 C CNN
	1    2700 4600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2200 4200 2200 4450
Wire Wire Line
	2200 4450 2550 4450
Wire Wire Line
	2550 4750 1700 4750
Text Notes 1450 4750 0    50   ~ 0
1023
Wire Wire Line
	4450 4600 2850 4600
Text Label 3250 4700 0    50   Italic 10
pc_in
Wire Wire Line
	4500 4850 4500 5000
Wire Wire Line
	4500 5000 4000 5000
Wire Wire Line
	4600 4850 4600 5150
Wire Wire Line
	4600 5150 4000 5150
Text Label 4000 5000 0    50   ~ 0
clk
Text Label 4000 5150 0    50   ~ 0
reset
Wire Wire Line
	5850 4850 5850 5050
Wire Wire Line
	5850 5050 5450 5050
Text Label 5450 5050 0    50   ~ 0
reset
Wire Wire Line
	9350 3350 9350 3650
Wire Wire Line
	9350 3650 9800 3650
Text Label 9450 3650 0    50   ~ 0
op_alu
Wire Wire Line
	3750 2900 3750 2650
Wire Wire Line
	3750 2650 4200 2650
Text Label 3800 2650 0    50   ~ 0
s_inc
Wire Wire Line
	2750 3050 2750 2750
Wire Wire Line
	2750 2750 2350 2750
Wire Wire Line
	1950 4050 1850 4050
Wire Wire Line
	1850 4050 1850 4250
Wire Wire Line
	1850 4250 1400 4250
Wire Wire Line
	2700 4350 2700 4250
Wire Wire Line
	2700 4250 3100 4250
Text Label 2350 2750 0    50   ~ 0
and_01_out
Text Label 1400 4250 0    50   ~ 0
s_int
Text Label 2900 4250 0    50   ~ 0
s_err
Wire Wire Line
	3600 4250 3600 4350
Wire Wire Line
	3600 4350 3850 4350
Text Label 3650 4350 0    50   ~ 0
reset
Wire Wire Line
	3400 4250 3400 4450
Wire Wire Line
	3400 4450 3700 4450
Text Label 3500 4450 0    50   ~ 0
clk
Wire Wire Line
	3100 4250 3100 3500
Wire Wire Line
	3100 3500 3650 3500
Wire Wire Line
	3650 3500 3650 3750
Wire Wire Line
	3350 3750 3350 3650
Wire Wire Line
	3350 3650 3150 3650
Text Label 3150 3650 0    50   ~ 0
s_ena
Wire Wire Line
	3500 3750 3500 3550
Wire Wire Line
	3500 3550 3200 3550
Text Label 3200 3550 0    50   ~ 0
s_rw
Wire Wire Line
	8150 3600 8150 3350
Wire Wire Line
	8150 3350 7800 3350
Text Label 7850 3350 0    50   ~ 0
clk
Wire Wire Line
	8550 3600 8550 3250
Wire Wire Line
	8550 3250 8250 3250
Text Label 8250 3250 0    50   ~ 0
we3
Wire Wire Line
	9350 3050 9350 2550
$Comp
L symbols:ffz U?
U 1 1 5C93FDDE
P 9350 2450
F 0 "U?" H 9578 2496 50  0001 L CNN
F 1 "zero" H 9578 2450 50  0000 L CNN
F 2 "" H 9350 2450 50  0001 C CNN
F 3 "" H 9350 2450 50  0001 C CNN
	1    9350 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 2500 9950 2500
Text Label 9850 2500 0    50   ~ 0
clk
Text Label 9350 2800 0    50   Italic 10
z_alu
Wire Wire Line
	9350 2350 9350 2050
Wire Wire Line
	9450 2450 9600 2450
Wire Wire Line
	9600 2450 9600 2400
Wire Wire Line
	9600 2400 9950 2400
Wire Wire Line
	9450 2400 9550 2400
Wire Wire Line
	9550 2400 9550 2300
Wire Wire Line
	9550 2300 9950 2300
Text Label 9700 2400 0    50   ~ 0
reset
Text Label 9650 2300 0    50   ~ 0
wez
Text Label 9350 2200 0    50   Italic 10
z
$Comp
L symbols:ffz U?
U 1 1 5C95A15B
P 1450 3600
F 0 "U?" V 1723 3600 50  0001 C CNN
F 1 "int" V 1724 3600 50  0000 C CNN
F 2 "" H 1450 3600 50  0001 C CNN
F 3 "" H 1450 3600 50  0001 C CNN
	1    1450 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 3000 4400 2450
Wire Wire Line
	4400 2450 1250 2450
Wire Wire Line
	1250 2450 1250 3600
Wire Wire Line
	1250 3600 1350 3600
Connection ~ 4400 3000
Wire Wire Line
	4400 3000 3900 3000
Wire Wire Line
	1400 3700 1400 3800
Wire Wire Line
	1400 3800 950  3800
Text Label 950  3800 0    50   ~ 0
s_int_set
Wire Wire Line
	1450 3700 1450 3850
Wire Wire Line
	1450 3850 1350 3850
Wire Wire Line
	1350 3850 1350 3900
Wire Wire Line
	1350 3900 950  3900
Text Label 950  3900 0    50   ~ 0
reset
Wire Wire Line
	1500 3700 1500 3800
Wire Wire Line
	1500 3800 1850 3800
Text Label 1650 3800 0    50   ~ 0
clk
$Comp
L symbols:and U?
U 1 1 5C965B09
P 2050 2750
F 0 "U?" H 1950 3075 50  0001 C CNN
F 1 "and01" H 1950 2983 50  0000 C CNN
F 2 "" H 2050 2750 50  0001 C CNN
F 3 "" H 2050 2750 50  0001 C CNN
	1    2050 2750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1950 2650 1350 2650
Wire Wire Line
	1950 2850 1350 2850
Text Label 1350 2650 0    50   ~ 0
s_ena
Text Label 1350 2850 0    50   ~ 0
~s_rw
Connection ~ 6400 4600
Text Label 6450 4700 0    50   ~ 0
address[09:04]
Wire Wire Line
	6400 4600 6400 4700
Wire Wire Line
	6400 4500 9300 4500
Connection ~ 6400 4500
Wire Wire Line
	6400 4500 6400 4600
$Comp
L symbols:mux U?
U 1 1 5C9BDA53
P 9650 4800
F 0 "U?" H 9650 5316 50  0001 C CNN
F 1 "mux06" H 9650 5224 50  0000 C CNN
F 2 "" H 9650 4800 50  0001 C CNN
F 3 "" H 9650 4800 50  0001 C CNN
	1    9650 4800
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 4000 7500 4200
Wire Wire Line
	6400 3000 6400 3400
Wire Wire Line
	9500 4650 9300 4650
Wire Wire Line
	9300 4650 9300 4500
Wire Wire Line
	9800 4800 10050 4800
Wire Wire Line
	10050 4300 10050 4800
Wire Wire Line
	6400 4200 7500 4200
Connection ~ 6400 4200
Wire Wire Line
	6400 4200 6400 4500
$Comp
L symbols:mux U?
U 1 1 5C9F9EDD
P 6900 3900
F 0 "U?" H 6900 4416 50  0001 C CNN
F 1 "mux07" H 6900 4325 50  0000 C CNN
F 2 "" H 6900 3900 50  0001 C CNN
F 3 "" H 6900 3900 50  0001 C CNN
	1    6900 3900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6750 3750 6400 3750
Connection ~ 6400 3750
Wire Wire Line
	6400 3750 6400 4050
Text Label 6450 3750 0    50   ~ 0
[07:04]
Wire Wire Line
	7200 3850 7200 3900
Wire Wire Line
	7200 3900 7050 3900
Wire Wire Line
	7200 3850 7950 3850
Wire Wire Line
	7200 3700 7200 3400
Wire Wire Line
	7200 3400 6400 3400
Wire Wire Line
	7200 3700 7950 3700
Connection ~ 6400 3400
Wire Wire Line
	6400 3400 6400 3750
Wire Wire Line
	6750 4050 6400 4050
Connection ~ 6400 4050
Wire Wire Line
	6400 4050 6400 4200
Text Label 6450 4050 0    50   ~ 0
[03:00]
Wire Wire Line
	6900 3650 6900 3200
Wire Wire Line
	6900 3200 7250 3200
Wire Wire Line
	9650 5050 9650 5150
Wire Wire Line
	9650 5150 10350 5150
Text Label 9900 5150 0    50   ~ 0
s_e_sel
Text Label 6950 3200 0    50   ~ 0
s_s_sel
Text Label 10050 4600 0    50   Italic 10
mux06_out
$Comp
L symbols:entrada U?
U 1 1 5C9D72E7
P 7350 4900
F 0 "U?" H 7350 5325 50  0001 C CNN
F 1 "entrada" H 7350 5233 50  0000 C CNN
F 2 "" H 7350 4900 50  0001 C CNN
F 3 "" H 7350 4900 50  0001 C CNN
	1    7350 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 3750 8850 3750
Wire Wire Line
	6400 4700 7150 4700
Wire Wire Line
	7150 4800 7000 4800
Wire Wire Line
	7000 4800 7000 4850
Wire Wire Line
	7000 4850 6400 4850
Wire Wire Line
	7150 4900 7000 4900
Wire Wire Line
	7000 4900 7000 4950
Wire Wire Line
	7000 4950 6400 4950
Wire Wire Line
	7150 5000 7000 5000
Wire Wire Line
	7000 5000 7000 5050
Wire Wire Line
	7000 5050 6400 5050
Wire Wire Line
	7150 5100 7000 5100
Wire Wire Line
	7000 5100 7000 5150
Wire Wire Line
	7000 5150 6400 5150
Wire Wire Line
	7550 4900 9350 4900
Wire Wire Line
	9350 4900 9350 4950
Wire Wire Line
	9350 4950 9500 4950
Text Label 6500 4850 0    50   ~ 0
p0
Text Label 6500 4950 0    50   ~ 0
p1
Text Label 6500 5050 0    50   ~ 0
p2
Text Label 6500 5150 0    50   ~ 0
p3
$Comp
L symbols:salida U?
U 1 1 5CA1CF29
P 7350 5600
F 0 "U?" H 7350 6015 50  0001 C CNN
F 1 "salida" H 7350 5923 50  0000 C CNN
F 2 "" H 7350 5600 50  0001 C CNN
F 3 "" H 7350 5600 50  0001 C CNN
	1    7350 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 5600 6600 5600
Wire Wire Line
	7550 5450 8150 5450
Wire Wire Line
	7550 5550 8150 5550
Wire Wire Line
	7550 5650 8150 5650
Wire Wire Line
	7550 5750 8150 5750
Wire Wire Line
	7150 5500 6300 5500
Wire Wire Line
	6300 5500 6300 4700
Wire Wire Line
	6300 4700 6400 4700
Connection ~ 6400 4700
Text Label 6500 5500 0    50   ~ 0
address[09:04]
Text Label 6700 5600 0    50   ~ 0
s_w_out
Wire Wire Line
	8850 3750 8850 6000
Wire Wire Line
	8850 6000 6950 6000
Wire Wire Line
	6950 6000 6950 5700
Wire Wire Line
	6950 5700 7150 5700
Connection ~ 8850 3750
Wire Wire Line
	8850 3750 8900 3750
Text Label 7700 5450 0    50   ~ 0
p4
Text Label 7700 5550 0    50   ~ 0
p5
Text Label 7700 5650 0    50   ~ 0
p6
Text Label 7700 5750 0    50   ~ 0
p7
$EndSCHEMATC
